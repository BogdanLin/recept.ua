import { KitchenPage } from './app.po';

describe('kitchen App', () => {
  let page: KitchenPage;

  beforeEach(() => {
    page = new KitchenPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
