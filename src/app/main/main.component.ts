import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http'; 
import 'rxjs/Rx'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  spaceDescs: Array<any>;

  constructor(private http: Http) {
    this.http.get('./assets/data.json')
      .map(response => response.json().recipies)
      .subscribe(res => this.spaceDescs = res)
  }

  likeMe(i) {
    if (this.spaceDescs[i].liked == 0) 
      this.spaceDescs[i].liked = 1;
    else
      this.spaceDescs[i].liked = 0;
  }

pageTitle =  'Main';
}
