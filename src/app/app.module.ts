import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { 
  MaterialModule , 
  MdInputModule, 
  MdCheckboxModule, 
  MdToolbarModule, 
  MdSidenavModule,
  MdButtonToggleModule,
  MdButtonModule,
  MdListModule,
  MdTabsModule
} from '@angular/material';

import 'hammerjs';

import { MainComponent } from './main/main.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NewRecipeComponent } from './new-recipe/new-recipe.component';

import { AppRoutingModule } from './app-routing.module';
import {  } from '@angular/material';
import {} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavigationComponent,
    NewRecipeComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpModule,
    MdToolbarModule,
    MdSidenavModule,
    MdButtonToggleModule,
    MdButtonModule,
    MdListModule,
    MdTabsModule,
    MdInputModule,
    MdCheckboxModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
