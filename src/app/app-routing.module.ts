import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import { NewRecipeComponent } from './new-recipe/new-recipe.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/main',
    pathMatch: 'full'
  },
  { 
    path: 'main', 
    component: MainComponent,
    data: {pageTitle: 'Main from rounts'}
  },
  {
    path: 'new-recipe',
    component: NewRecipeComponent,
    data: {pageTitle: 'NewRecipe from rounts'}
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
