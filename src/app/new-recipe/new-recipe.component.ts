import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http'; 
import 'rxjs/Rx'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.scss']
})
export class NewRecipeComponent {

  ingridients: Array<any>;
  email: string
  rForm: FormGroup;
  post: any;
  description: string;
  name: string;
  author: string;
  ingridient: Array<any>
  recipe: string;
  preparation: number;
  cook: number;
  readyIn: number;
  password: string;
  nameError: string = "Name field is required";

  constructor(private http: Http, private fb: FormBuilder) {
    this.http.get('./assets/ingridients.json')
      .map(response => response.json().ingridients)
      .subscribe(res => this.ingridients = res);
    this.rForm =fb.group(
      {
        'author': ['author', Validators.required],
        'name': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
        'email': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30), Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")])],
        'description': [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(30), Validators.maxLength(500)])],
        'recipe': [null, Validators.required],
        'preparation': [null, Validators.required],
        'cook': [null, Validators.required],
        'readyIn': [null, Validators.required],
        'ingridient': [null, Validators.required],
        'password': [null, Validators.compose([Validators.required])]
      }
    )
  }
  addPost(post) {
    this.author = post.author;
    this.name = post.name;
    this.description = post.description;
    this.recipe = post.recipe;
    this.preparation = post.preparation;
    this.cook = post.cook;
    this.readyIn = post.readyIn;
    this.ingridient = post.ingridient;
  }

  ngOnInit() {
  }

}
